## Resume Website for Karl Haworth
### Print stlyes produce PDF resume. Best printed in Safari.

[![Netlify Status](https://api.netlify.com/api/v1/badges/1cc341c5-8fbf-45f7-8b7d-8ad204a1a5a8/deploy-status)](https://app.netlify.com/sites/karlhaworth/deploys)

#### Technologies Utilized
- [Bootstrap](https://getbootstrap.com)
- [Sass](https://sass-lang.com)
- [Jekyll](http://jekyllrb.com)

#### RUN
```bash
$ bundle exec jekyll serve
```

#### INSTALL BASE
```bash
$ gem install bundler jekyll
$ jekyll new karlhaworth-com_jekyll
$ cd karlhaworth-com_jekyll
```